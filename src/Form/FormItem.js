import styles from './FormItem.module.css'
const FormItem = (props) => {
  const removeItem = () => {
    localStorage.setItem('enterList', '1')
  }
  return (
    <li className={styles['form-item']} onClick={removeItem}>
      {props.children}
    </li>
  )
}
export default FormItem
