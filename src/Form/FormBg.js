import styles from 'styled-components'
const FormBg = styles.div`
position: fixed;
top: 0;
left: 0;
height: 100vh;
width: 100vw;
background: rgba(60, 72, 82, 0.5);
filter: blur(100px);
backdrop-filter: blur(50px);
z-index:50`
export default FormBg
