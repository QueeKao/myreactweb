import React, { useEffect, useReducer } from 'react'
import styled from './FormInput.module.css'
import Button from '../UI/Button'
const userReducer = (preState, action) => {
  if (action.type === 'USER_INPUT') {
    return {
      value: action.val,
      isValid: action.val.trim().length < 5
    }
  }
  return { value: '', isValid: false }
}
const numberReducer = (preState, action) => {
  if (action.type === 'NUMBER_INPUT') {
    return {
      value: action.val,
      isValid: action.val.trim().length < 2
    }// its mean right now I am dealing with this type of method
  } // And decide what kind of value you should give
  return { value: '', isValid: false }
}
const FormInput = (props) => {
  // const [inputUser, setInputUser] = useState("");
  // const [inputNumber, setInputNumber] = useState("");
  const [numberState, dispatchNumber] = useReducer(numberReducer, {
    value: '',
    isValid: null
  })
  const [userState, dispatchUser] = useReducer(userReducer, {
    value: '',
    isValid: null
  })
  useEffect(() => {
    // console.log('keystroke');
  }, [userState.isValid, numberState.isValid])
  // const UserChange = (e) => {
  //   setInputUser(e.target.value);
  // };
  const userChange = (e) => {
    dispatchUser({ type: 'USER_INPUT', val: e.target.value })
  }
  const numberChange = (e) => {
    dispatchNumber({ type: 'NUMBER_INPUT', val: e.target.value })
  }
  // const userValid = (e) => {
  //   dispatchUser({ type: "USER_VALID" });
  // };
  // const NumberChange = (e) => {
  //   setInputNumber(e.target.value);
  // };
  console.log(userState.isValid)
  const submitForm = (e) => {
    e.preventDefault()
    const submitData = {
      User: userState.value,
      age: +numberState.value,
      key: Math.random().toString()
    }
    if (
      userState.value.trim().length < 5 ||
      numberState.value.trim().length < 2 ||
      +numberState.val < 0
    ) {
      props.onError(true)
      return
    }

    props.onSubmitData(submitData)
    dispatchUser('')
    dispatchNumber('')
    // this reset is not working because we didnt change the cur use value
    // and we cant fix to set cur input value
  }

  return (
    <div>
      <form onSubmit={submitForm}>
        <div className={styled['form-control']}>
          <input
            type='text'
            onChange={userChange}
            className={`${styled.user} ${userState.isValid ? styled.invaild : ''
              }`}
            id='username'
            value={userState.value}
          ></input>
          <label htmlFor='username' className={styled.userLabel}>
            Username
          </label>
          {/* for JSX we need htmlFor */}
          <input
            id='age'
            type='number'
            onChange={numberChange}
            className={`${styled.age} ${numberState.isValid ? styled.invaild : ''
              }`}
            value={numberState.value}
          ></input>
          <label htmlFor="age" className={styled.ageLabel}>
            Age(Years)
          </label>
          <Button type="submit">Add New</Button>
        </div>
      </form>
    </div>
  )
}

export default FormInput
