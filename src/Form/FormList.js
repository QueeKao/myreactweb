import styles from './FormList.module.css'
import React, { Component } from 'react'
import FormItem from './FormItem'
class FormList extends Component {
  componentDidCatch () { }
  componentDidUpdate () { }
  componentWillUnmount () { }
  componentDidMount () { }
  render () {
    // console.log('Formlist run');
    return (
      <ul className={styles['form-list']}>
        {this.props.items.map((dataSub) => (
          <FormItem key={dataSub.key} age={dataSub.age} name={dataSub.user}>
            {dataSub.User} {dataSub.age} years old
          </FormItem>
        ))}
      </ul>
    )
  }
}
// const FormList = (props) => {
//   console.log("Formlist run");
//   return (
//     <ul className={styles["form-list"]}>
//       {props.items.map((dataSub) => (
//         <FormItem
//           key={Math.random().toString()}
//           age={dataSub.age}
//           name={dataSub.user}
//         >
//           {dataSub.User} {dataSub.age} years old
//         </FormItem>
//       ))}
//     </ul>
//   );
// };
export default React.memo(FormList)
