import React from 'react'
import ReactDom from 'react-dom'
import Button from '../UI/Button'
import styles from './FormPopup.module.css'
import Card from '../UI/Card'
import FormBg from './FormBg'
const FormPopup = (props) => {
  console.log('popup RuN')
  return (
    <React.Fragment>
      {ReactDom.createPortal(
        <FormBg onClick={props.onConfirm}></FormBg>,
        document.getElementById('overlay')
      )}
      {ReactDom.createPortal(
        <Card>
          <div className={styles['card__header']}>
            <h3>Invalid Input</h3>
          </div>
          <p>Please enter valid name and age(non-empty values)</p>
          <Button onClick={props.onConfirm} className={styles.button}>
            OKAY
          </Button>
        </Card>,
        document.getElementById('overlay')
      )}
    </React.Fragment>
  )
}
export default FormPopup
