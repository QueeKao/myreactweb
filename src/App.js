import React, { useState } from 'react'
import './App.css'
import FormInput from './Form/FormInput'
import FormPopup from './Form/FormPopup'
import FormList from './Form/FormList'
// class App extends Component {
//   constructor() {
//     super();
//     this.state = {
//       error: false,
//       inputData: [{ age: 23, User: "Max", key: Math.random().toString() }],
//     };
//   }
//   SubmitData(enterText) {
//     this.setState((enterData) => {
//       console.log(enterData);
//       this.state.inputData.unshift(enterText);
//       console.log(enterText);
//       return enterData;
//       // const updateData = [...enterData];
//       //       updateData.unshift(enterText);
//       //       return updateData;
//     });
//   }
//   errorHandler() {
//     this.setState({ error: null });
//   }
//   render() {
//     console.log("App run");
//     return (
//       <React.Fragment>
//         {this.state.error && <FormPopup onConfirm={this.errorHandler} />}
//         <section id="goal-form">
//           <FormInput
//             onError={this.errorHandler.bind(this)}
//             onSubmitData={this.SubmitData.bind(this)}
//           ></FormInput>
//         </section>
//         <section id="goal-input">
//           <FormList items={this.state.inputData}></FormList>
//         </section>
//       </React.Fragment>
//     );
//   }
// }
const App = () => {
  console.log('App run')
  const [inputData, setData] = useState([
    {
      age: 23,
      User: 'Max',
      key: Math.random().toString()
    }
  ])
  const [error, setError] = useState(false)

  const SubmitData = (enterText) => {
    setData((enterData) => {
      const updateData = [...enterData]
      console.log(enterData)
      console.log(enterText)
      updateData.unshift(enterText)
      console.log(updateData)
      return updateData
    })
  }
  const errorHandler = () => {
    setError(null)
  }
  // const statement = () => {};
  return (
    <React.Fragment>
      {error && <FormPopup onConfirm={errorHandler} />}
      <section id="goal-form">
        <FormInput onError={setError} onSubmitData={SubmitData}></FormInput>
      </section>
      <section id="goal-input">
        <FormList items={inputData}></FormList>
      </section>
    </React.Fragment>
  )
}

export default App
