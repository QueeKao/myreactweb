import styles from './Card.module.css'
function Card (props) {
  // const classes = styles.card + props.className;
  // const showFormPop = (props) => {
  //   const classes = styles.card + props.showFrom.opacity;
  //   return classes;
  // };
  return <div className={styles.card}>{props.children}</div>
}
export default Card
